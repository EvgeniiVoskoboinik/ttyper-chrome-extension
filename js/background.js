var tt = new TT();
tt.init();

function TT(){
    var socket
        , currentTabId
        , currentTabUrl
        , isAuth = false
        , isConnected = false
        , username = ''
        , tabArr = {} // tabArr[tabId] = room
        , oldTabArr = {}
        , hostArr = ['www.youtube.com'] //list of sites for including query to room name
        , blackList = ['localhost', 'ttyper.com', 'www.google.ru','www.yandex.ru','mail.google.com','mail.yandex.ru']
        , checkedList = []
        , storage = {}
        , sendMessage = chrome.tabs.sendMessage
        , commentsLimit = 30;
    //var url = 'http://127.0.0.1:3000';
    var url = 'http://ttyper.com';

    function makePostRequest(){

        var timerId;
        var xhr = new XMLHttpRequest();
        xhr.withCredentials = true;

        xhr.open("POST", url + "/checkin", true);
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {

                if(xhr.status == 200) {
                    clearTimeout(timerId);
                    connect();
                }
                else
                    timerId = setTimeout(makePostRequest, 10000);
            }
        };
        xhr.send();
    }

    function subscribeRoom(room){
        if (isAuth)
            socket.emit ('subscribe', room);
    }

    function saveToStorage(room, comments){
        storage[room] = storage[room].concat(comments);

    }

    function getStoredComments(room, callback){

        if(typeof(storage[room]) != "undefined")
            return callback(null, storage[room]);

        return callback('error');
    }

    function getCommentsFromDb(room, cid){
        var id = cid || '000000000000000000000000';

        socket.emit("comments from db", {
                tabUrl: room,
                id: id,
                newer: true
            },
            function(err, comments){
                var len = comments.length;

                if (!err && len != 0){

                    var lock = 0;

                    if (id == '000000000000000000000000' && len == commentsLimit)
                        lock = 1;

                    comments = comments.reverse();

                    saveToStorage(room, comments);
                    sendToTabs(room, comments, lock);
                }
            });
    }

    function getOlderCommentsFromDb(room, cid, callback){

        socket.emit("comments from db", {
                tabUrl: room,
                id: cid,
                newer: false
            },
            function(err, comments){
                var len = comments.length;

                if (!err && len != 0){

                    var lock = 1;

                    if (len < commentsLimit)
                        lock = 0;

                    callback(comments, lock);
                }
            });
    }

    function parseUrl(url){

        var u = new Url (url);

        if ((u.protocol == 'http' || u.protocol == 'https') && blackList.indexOf(u.host) === -1){
            if (hostArr.indexOf(u.host) === -1)
                var room = u.host + u.path;
            else
                room = u.host + u.path + u.query;
            return room;
        }
        return null;
    }

    function checkArrValue(arr, val){
        for(var key in arr){
            if(arr[key] == val) return key;
        }
        return null;
    }

    function checkComments(room, tabId, leave){
        getStoredComments(room, function(err, comments){
            if(!err){
                var len = comments.length;

                if(len >= commentsLimit)
                    var lock = 1;
                else
                    lock = 2;

                sendMessage(tabId,{
                    type: "create form",
                    data: comments,
                    leave: leave,
                    lock: lock,
                    username: username
                });

                if(checkedList.indexOf(room) === -1){
                    if(len != 0)
                        var cid = comments[len-1]._id;

                    if(isConnected){
                        getCommentsFromDb(room, cid);
                        checkedList.push(room);
                        subscribeRoom(room);
                    }
                }
            }
        });
    }

    function workWithOldTabArr(){
        if(checkArrValue(oldTabArr, currentTabUrl) != null){
            getStoredComments(currentTabUrl, function(err, comments){
                if(!err){
                    var len = comments.length;

                    if(len != 0)
                        var cid = comments[len-1]._id;

                    getCommentsFromDb(currentTabUrl, cid);
                    subscribeRoom(currentTabUrl);
                }
            });

            checkedList.push(currentTabUrl);
            for(var r in oldTabArr){ //удалить все вкладки с таким же room
                if(oldTabArr[r] == currentTabUrl)
                    delete oldTabArr[r];//есть ли возможность, что currentTabUrl изменится к этому времени?
            }
        }
    }

    function sendToTabs(room, comments, lock){
        for (var r in tabArr){

            if (tabArr[r] == room) {

                var tabId = parseInt(r);

                chrome.tabs.get(tabId, function(tab){

                    if(typeof(tab) != "undefined")

                        chrome.tabs.sendMessage(tab.id, {
                            type: "new comments",
                            data: comments,
                            lock: lock
                        });
                });
            }
        }
    }

    function informTabs(auth){ //except one tab
        for (var r in tabArr){

            var tabId = parseInt(r);
            chrome.tabs.get(tabId, function(tab){

                if(typeof(tab) != "undefined")
                    chrome.tabs.sendMessage(tab.id, {
                        type: "changed auth state",
                        auth: auth,
                        username: username
                    });
            });
        }
    }

    function connect(){
        socket = io.connect(url);
        socket
            .on('connect', function(){
                oldTabArr = JSON.parse(JSON.stringify(tabArr));
                checkedList = [];
                isConnected = true;
                workWithOldTabArr();
            })

            .on('auth', function(data){
                if (isAuth = data.isAuth) username = data.username;
                informTabs(isAuth);
            })

            .on('comment', function(data){
                saveToStorage(data.tabUrl, data.comments);
                sendToTabs(data.tabUrl, data.comments, false);
            })

            .on('logout', function(){
                isAuth = false;
                informTabs(false);
            })

            .on('disconnect', function(){
                isConnected = false;
            });
    }

    this.init = function(){
        makePostRequest();

        chrome.runtime.onMessage.addListener(
            function(request, sender, sendResponse) {
                switch (request.type){

                    case "send comment":{
                        socket.emit ('comment', {
                                anonymously: request.anonymously,
                                comment: request.text,
                                response_to: request.response_to,
                                tabUrl: tabArr[sender.tab.id]
                            },
                            function(err, data){
                                if (!err){
                                    saveToStorage(data.tabUrl, data.comments);
                                    sendToTabs(data.tabUrl, data.comments, false);
                                }
                                sendResponse(err);
                            });
                        return true;
                    } break;

                    case "check authorisation state":
                        sendResponse({'isAuth':isAuth, 'isConnected': isConnected});
                        break;

                    case "makeAuth":{
                        socket.emit('makeAuth', request.data, function(err){
                            if (!err){
                                isAuth = true;
                                username = request.data.username;
                                informTabs(true);
                            }
                            sendResponse(err);
                        });
                        return true;
                    } break;

                    case "makeReg":{
                        socket.emit('makeReg', request.data, function(err){
                            sendResponse(err);
                        });
                        return true;
                    } break;

                    case "load comments":{
                        getOlderCommentsFromDb(tabArr[sender.tab.id],request.cid, function(comments, lock){

                            var response = {
                                comments: comments,
                                lock: lock
                            };

                            sendResponse(response);
                        });
                        return true;
                    } break;

                    case "one comment":{

                        var room = tabArr[sender.tab.id];
                        getStoredComments(room, function(err, comments){
                            if(!err){
                                for(var r in comments){
                                    if(comments[r]['_id'] == request.data.id){
                                        var response = {
                                            err: null,
                                            comment: comments[r]
                                        };
                                        return sendResponse(response);
                                    }
                                }
                            }
                            socket.emit('one comment', request.data, function(err, comment){//cache this comment
                                var response = {
                                    err: err,
                                    comment: comment[0]
                                };
                                return sendResponse(response);
                            });
                        });
                        return true;
                    } break;

                    case "spam":{
                        socket.emit('spam', request.data);
                    } break;

                    case "logout":{
                        socket.emit('logout', function(err){
                            if (!err) {
                                isAuth = false;
                                username = '';
                                informTabs(false);
                            }
                            return sendResponse(err);
                        });
                        return true;
                    } break;
                }
            });

        chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
            if (changeInfo.status == 'complete'){

                var room = parseUrl(tab.url);

                if (room != null){
                    if(tabId == currentTabId)
                        currentTabUrl = room;

                    if (tabArr[tabId] == room)
                        checkComments(room, tabId, true);

                    else {
                        if (checkArrValue(tabArr, room) == null)
                            storage[room] = storage[room] || [];

                        checkComments(room, tabId, false);
                        tabArr[tabId] = room;
                    }
                }
            }
        });

        chrome.tabs.onActivated.addListener(function(info){
            currentTabId = info.tabId;

            if(typeof(tabArr[currentTabId]) != "undefined"){
                currentTabUrl = tabArr[currentTabId];
                workWithOldTabArr();
            }
            else
                currentTabUrl = '';
            //проверяем, есть ли эта вкладка в массиве oldTabArr
            // если да, то подписываемся на соответствующий рум и запрашиваем комментарии
            // если нет, то только сохраняем currentTab на случай дисконнекта
        });

        chrome.tabs.onRemoved.addListener(function(tabId) {
            delete tabArr[tabId];
        });
    };

    return this;
}
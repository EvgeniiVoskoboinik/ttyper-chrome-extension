var ttSendMessage = chrome.runtime.sendMessage;
var ttCommentFormObj;

function createCommentForm(callback){
    $("body").append(
     '<div id="ucs_wrap">' +
         '<div id="ucs_header">' +
             '<div id="ucs_comment_notification"></div>' +
             '<div id="ucs_greeting"></div>' +
             '<div id="ucs_minimize_button"></div>' +
             '<div id="ucs_restore_button"></div>' +
         '</div>' +
         '<div id="ucs_main">' +
             '<div id="ucs_spam_button">Пожаловаться</div>' +
             '<div id="ucs_response_button">Ответить</div>' +
             '<div class="ucs_scrollbar">' +
                 '<div class="ucs_track">' +
                     '<div class="ucs_thumb">' +
                         '<div class="ucs_end">' +
                         '</div>' +
                     '</div>' +
                 '</div>' +
             '</div>' +
             '<div class="ucs_viewport">' +
                 '<div class="ucs_overview"></div>' +
                 '<div id="ucs_info_bar"></div>' +
             '</div>' +
         '</div>' +
         '<div id="ucs_response_to_block"></div>' +
         '<div id="ucs_send" contenteditable="true"></div>' +
         '<div id="ucs_footer">' +
         '<div id="ucs_anonym_label">Анонимно</div>' +
         '<input type="checkbox" id="ucs_anonym_checkbox"/>' +
         '<div id="ucs_live_label">Live</div>' +
         '<input type="checkbox" id="ucs_live_checkbox"/>' +
         '<div id="ucs_in_out">Войти</div>' +
         '</div>' +
     '</div>');

    ttCommentFormObj = new CommentFormObj();
    ttCommentFormObj.init();
    callback();
}

function CommentFormObj(){
    var anonymously = false
        , response_to_id = null
        , info_bar_timeout
        , lock = 2 // 1 - ready, 2 - lock
        , comment_notification = [$("#ucs_comment_notification"), false]
        , wrap =  $("#ucs_wrap")
        , main = $("#ucs_main")
        , ucs_send = $('#ucs_send')
        , response_to_block = $('#ucs_response_to_block')
        , footer = $('#ucs_footer')
        , min_button = $('#ucs_minimize_button')
        , res_button = $('#ucs_restore_button')
        , scroll;

    this.init = function(){
        main.tinyscrollbar();
        scroll = main.data("plugin_tinyscrollbar");

        wrap.draggable({
            zIndex: 9999999990
            , handle:"#ucs_header"
            , cancel:"#ucs_close_button,#ucs_minimize_button,#ucs_restore_button"
            , scroll: false
        });

        wrap.resizable({
            alsoResize: "#ucs_main"
            , handles: "s, w, sw"
            , minHeight: 245
            , minWidth: 133
        });

        wrap.resizable({
            resize: function() {
                scroll.update('relative');
                wrap.css("height", "")
                    .css("position", "");
            }
        });

        wrap.resizable("disable");

        invokeListeners();
    };

    this.getAnonymously = function(){
        return anonymously;
    };

    this.getResponse_to_id = function(){
        return response_to_id;
    };

    this.clearResponse_to_id = function(){
        response_to_id = null;
    };

    this.setLock = function(val){
        lock = val;
    };

    this.setInfoBarTimeout = function(func, time){
        clearTimeout(info_bar_timeout);
        info_bar_timeout = setTimeout(func, time);
    };

    this.clearInfoBarTimeout = function(){
        clearTimeout(info_bar_timeout);
    };

    this.hideCommentNotification = function(){
        if(comment_notification[1]){
            comment_notification[0].removeClass('ucs_comment_notification_on');
            comment_notification[1] = false;
        }
    };

    this.showCommentNotification = function(){
        if(!comment_notification[1]){
            comment_notification[0].addClass('ucs_comment_notification_on');
            comment_notification[1] = true;
        }
    };

    this.getObjVar = function(v){
        return this[v];
    };

    function invokeListeners(){

        $("#ucs_anonym_checkbox").change(function(){
            ucs_send.focus();
            anonymously = this.checked;
        });

        $(document).tooltip({
            items:'.ucs_response_to'
            , tooltipClass: "ucs_tooltip_class"
            , position: { my: "left+15 top", at: "right center" }
            , content: function(callback) {
                var el = $(this);
                var TTtmr = setTimeout( function() {

                    var data = {id: el.attr('res_to')};

                    ttSendMessage({type: "one comment", data: data}, function(response){
                        if(!response.err){
                            createResponseBlock(response.comment, function(data){
                                return callback(data);
                            });
                        }
                    });
                }, 500);
                el.mouseleave( function() { clearTimeout(TTtmr); } );
            }
        });

        function createResponseBlock(comment, callback){
            var data =
                '<div class="ucs_response">' +
                    '<div class="ucs_response_nickname">' +
                        comment.from +
                    '</div>' +
                    '<div class="ucs_response_time">' +
                        formatDate(comment.time) +
                    '</div>' +
                    '<div class="ucs_clear"></div>' +
                    '<div class="ucs_response_text">' +
                        comment.text +
                    '</div>' +
                '</div>';
            callback(data);
        }

        min_button.click(function() {
            main.hide();
            ucs_send.hide();
            response_to_block.hide();
            footer.hide();
            min_button.hide();
            res_button.show();
            wrap.css("height", "");
            wrap.resizable("disable");
        });

        res_button.click(function() {
            main.show();
            ucs_send.show();
            response_to_block.show();
            footer.show();
            min_button.show();
            res_button.hide();
            wrap.resizable("enable");
            scroll.update('bottom');
            ucs_send.focus();
        });

        $('#ucs_response_button').click(function(){
            var selectedComment = $('.ucs_selected_comment');
            response_to_id = selectedComment.attr('id');
            ucs_send.focus();
            response_to_block.html('To: ' + $('.ucs_selected_comment .ucs_nickname').text() + '<span> (X)</span>')
                             .addClass('ucs_response_to_block_hover');
            selectedComment.removeClass('ucs_selected_comment');
            $(this).css('display', 'none');
            $('#ucs_spam_button').css('display', 'none');
        });

        $('#ucs_spam_button').click(function(){
            var el = $(this);

            ttSendMessage({type: "check authorisation state"}, function(response){

                if (!response.isConnected)
                    showErrNotification('Отсутствует соединение с сервером');
                else{
                    var selectedComment = $('.ucs_selected_comment');
                    var data = {id: selectedComment.attr('id')};

                    ttSendMessage({type: "spam", data: data});
                    selectedComment.removeClass('ucs_selected_comment');
                    $('#ucs_response_button').css('display', 'none');
                    el.css('display', 'none');
                    showPositiveNotification('Жалоба отправлена');
                }
            });
        });

        response_to_block.click(function(){
            response_to_id = null;
            response_to_block.text('').removeClass('ucs_response_to_block_hover');
            ucs_send.focus();
        });

        $('#ucs_in_out').click(function(){
            ttSendMessage({type: "check authorisation state"}, function(response){
                if (!response.isAuth) createAuthRegForm();
                else logOut();
            });
        });

        ucs_send.keyup(function(e){ check_charcount(e); });

        ucs_send.keydown(function(e){
            if (e.which == 13 && (e.ctrlKey == false)){
                e.preventDefault();
                checkAuth();
            }
            else
                check_charcount(e);
        });

        function check_charcount(e){
            if(e.which != 8 && ucs_send.text().length > 450)
                e.preventDefault();
        }

        main.bind("move", function(){
            if (scroll.contentPosition == 0 && lock == 1){
                lock = 2;

                ttSendMessage({
                        type: "load comments"
                        , cid: $('.ucs_comment').attr('id')
                    },
                    function(response){
                        var contentSize1 = scroll.contentSize;
                        var comments = response.comments;

                        for(var i = 0, len = comments.length; i<len; i++){
                            var comment = comments[i];

                            createCommentBlock(comment._id, comment.from, comment.text, comment.time, comment.response_to, false);
                        }

                        scroll.update();

                        var contentSize2 = scroll.contentSize;

                        scroll.update(contentSize2 - contentSize1);

                        lock = response.lock || lock;
                    });
            }
        });


    }
    return this;
}

function createAuthRegForm(){
    $("body").append(
        '<div id="ucs_auth_wrap">'+
            '<div id="ucs_auth_close_button">Закрыть</div>'+
            ' <div id="ucs_auth_reg_forms" style="top: 50px;">'+
                '<div class="ucs_label">Войти</div>'+
                '<div>'+
                    '<form action method="post" autocomplete="off" id="ucs_auth_form">'+
                        '<input id="ucs_auth_username" name="ucs_username" value="" type="text" placeholder="Логин"/>'+
                        '<input id="ucs_auth_password" name="ucs_password" value="" type="password" placeholder="Пароль"/>'+
                        '<input type="submit" id="ucs_auth_send_button" value="Отправить"/>'+
                    '</form>'+
                    '<div id="ucs_auth_form_notification"></div>'+
                '</div>'+
                '<div class="ucs_label">Зарегистрироваться</div>'+
                '<div>'+
                    '<form action method="post" autocomplete="off" id="ucs_reg_form">'+
                        '<input id="ucs_reg_username" name="ucs_username" value="" type="text" placeholder="Логин"/>'+
                        '<input id="ucs_reg_password1" name="ucs_password1" value="" type="password" placeholder="Пароль"/>'+
                        '<input id="ucs_reg_password2" name="ucs_password2" value="" type="password" placeholder="Повторите пароль"/>'+
                        '<input type="submit" id="ucs_reg_send_button" value="Отправить"/>'+
                    '</form>'+
                    '<div id="ucs_reg_form_notification"></div>'+
                '</div>'+
            '</div>'+
        '</div>'
    );

    function checkRegForm(login, pass1, pass2, callback){
        var pass_regexp =  /^[A-z0-9_-]{1,}$/i
            , login_regexp =  /^[A-z0-9_-]{1,}$/i;

        if(login === "" || pass1 === "" || pass2 === "")
            return callback("Не все поля заполнены");

        else if(login.length > 20 || login.length < 3)
            return callback("Допустимая длина имени 3-20 символов");

        else if(pass1.length < 5)
            return callback("Пароль не должен быть короче 5 символов");

        else if(!pass_regexp.test(pass1) || !login_regexp.test(login))
            return callback("Допускаются только латинские буквы, цифры, нижнее подчеркивание, тире");

        else if (pass1 != pass2)
            return callback("Пароли не совпадают");

        return callback();
    }

    function checkAuthForm(login, pass, callback){
        var pass_regexp =  /^[A-z0-9_-]{1,}$/i;

        if(login === "" || pass === "")
            return callback("Не все поля заполнены");

        else if(pass.length < 5 || !pass_regexp.test(pass))
            return callback("Неверный пароль");

        return callback();
    }

    $( "#ucs_auth_reg_forms" ).accordion({
        heightStyle: "content"
    });

    $("#ucs_auth_form").submit(function (e) {
        e.preventDefault();
        var login = $("#ucs_auth_username").val().trim()
            , pass = $("#ucs_auth_password").val();

        checkAuthForm(login, pass, function(err){
            if (err)
                showAuthErrNotification(err);
            else{
                var formData = {
                    "username":login
                    , "password":pass
                };
                makeAuth(formData);
            }
        });
    });

    $("#ucs_reg_form").submit(function (e) {
        e.preventDefault();
        var login = $("#ucs_reg_username").val().trim()
            , pass1 = $("#ucs_reg_password1").val()
            , pass2 = $("#ucs_reg_password2").val();

        checkRegForm(login, pass1, pass2, function(err){
            if(err)
                showRegErrNotification(err);
            else{
                var formData = {
                    "username":login
                    , "password1":pass1
                    , "password2":pass2
                };
                makeReg(formData);
            }
        });
    });

    $(document).click(function (e) {
        if ($(e.target).closest('#ucs_auth_reg_forms, #ucs_wrap').length == 0) {
            $('#ucs_auth_wrap').hide('fast', function(){ $(this).remove(); });
            $(this).off('click');
        }
    });
}

function createCommentBlock(id, from, text, time, res_to, after){

    var table = document.createElement('table')
        , tbody = document.createElement('tbody')
        , trUp = document.createElement('tr')
        , trDown = document.createElement('tr')
        , nickname = document.createElement('td')
        , response_to = document.createElement('td')
        , comment_time = document.createElement('td')
        , comment_text = document.createElement('td')
        , comment = document.createElement('div')
        , root = document.getElementsByClassName('ucs_overview');

    nickname.setAttribute("class", "ucs_nickname");
    nickname.setAttribute("style", "font-size:11px");
    comment_time.setAttribute("class", "ucs_comment_time");
    comment_time.setAttribute("style", "font-size:10px");
    comment_text.setAttribute("class", "ucs_comment_text");
    comment_text.setAttribute("colspan", 3);
    comment_text.setAttribute("style", "font-size:11px");
    comment.setAttribute("class", "ucs_comment");
    comment.setAttribute("id", id);

    nickname.textContent = from;
    comment_time.textContent = formatDate(time);
    comment_text.textContent = text;

    if(res_to){
        response_to.setAttribute("res_to", res_to);
        response_to.setAttribute("class", 'ucs_response_to');
    }

    trUp.appendChild(nickname);
    trUp.appendChild(response_to);
    trUp.appendChild(comment_time);
    trDown.appendChild(comment_text);
    tbody.appendChild(trUp);
    tbody.appendChild(trDown);
    table.appendChild(tbody);
    comment.appendChild(table);

    if (after)
        root[0].appendChild(comment);
    else
        $('.ucs_overview').prepend(comment);

    $('#'+id).on('click', function(e){
        var el = $(this);

        if(e.target.className == 'ucs_comment_text'){
            if(el.hasClass("ucs_selected_comment")){
                el.removeClass("ucs_selected_comment");
                $('#ucs_response_button').css('display', 'none');
                $('#ucs_spam_button').css('display', 'none');
            }
            else{
                $('.ucs_selected_comment').removeClass('ucs_selected_comment');
                el.addClass("ucs_selected_comment");
                $('#ucs_response_button').css('display', 'block');
                $('#ucs_spam_button').css('display', 'block');
            }
        }
    });
}

function formatDate(date) {
    var now = new Date()
        , time = new Date(date)
        , min = time.getMinutes()
        , hh = time.getHours()
        , dd = time.getDate()
        , mm = time.getMonth()+1
        , yy = time.getFullYear() % 100
        , today = new Date(now.getFullYear(), now.getMonth(), now.getDate()).valueOf()
        , time1 = new Date(time.getFullYear(), time.getMonth(), time.getDate()).valueOf();

    if(today === time1){
        if ( min < 10 ) min = '0' + min;
        return hh+':'+min;
    }

    if ( mm < 10 ) mm = '0' + mm;
    return dd+'.'+mm+'.'+yy;
}

$.fn.getPreText = function () {
    var ce = $("<pre />").html(this.html());
    ce.find("div").replaceWith(function(){
        if (this.innerText != '\n') return "\n" + this.innerText;
    });
    return ce.text();
};

function logOut(){
    ttSendMessage({type:'logout'}, function(err){
        if(err) showErrNotification(err);
        else {
            logOutChangeForm();
            showPositiveNotification('Вы успешно вышли');
        }
    });
}

function logOutChangeForm(){
    $('#ucs_greeting').text('');
    $('#ucs_in_out').text('Войти');
}

function logInChangeForm(username){
    $('#ucs_greeting').text('Привет, ' + username);
    $('#ucs_in_out').text('Выйти');
}

function makeAuth(data){
    ttSendMessage({type: "makeAuth", data: data}, function(err){
        if(!err){
            logInChangeForm(data.username);
            showPositiveNotification("Авторизация прошла успешно");
            $("#ucs_auth_wrap").hide('fast', function(){ $(this).remove(); });
        }
        else
            showErrNotification(err);
    });
}

function makeReg(data){
    ttSendMessage({type: "makeReg", data: data}, function(err){
        if(!err){
            showPositiveNotification("Вы успешно зарегистрировались");
            $("#ucs_reg_form")[0].reset();
            $( "#ucs_auth_reg_forms" ).accordion( "option", "active", 0 );
        }
        else
            showErrNotification(err);
    });
}

function sendComment(){
    var check_con_timeout;
    if(!sendComment.send_lock){
        sendComment.send_lock = true;//against comments resend

        var ucs_send = $("#ucs_send");
        var comment = ucs_send.getPreText().replace(/\xA0+/g, '').replace(/[ ]{2,}/g, ' ').replace(/[\n]{2,}/g, '\n').trim();

        if(comment !== ''){
            var ucs_info_bar = $('#ucs_info_bar');
            var response_to_id = ttCommentFormObj.getResponse_to_id();

            ucs_info_bar.css("background-color", "#dddddd");

            ttCommentFormObj.setInfoBarTimeout(function(){
                ucs_info_bar.html('Отправляю...');
                check_con_timeout = setTimeout(function(){
                    showErrNotification('Отсутствует соедиенение с сервером');
                    sendComment.send_lock = false;
                }, 10000)
            }, 1000);

            ttSendMessage({
                    type: "send comment"
                    , text: comment
                    , anonymously: ttCommentFormObj.getAnonymously()
                    , response_to: response_to_id
                },
                function(err){
                    ttCommentFormObj.clearInfoBarTimeout();
                    clearTimeout(check_con_timeout);
                    ucs_info_bar.empty();

                    if(!err){
                        ucs_send.empty();
                        if (response_to_id !== null){
                            ttCommentFormObj.clearResponse_to_id();
                            $('#ucs_response_to_block').text('').removeClass('ucs_response_to_block_hover');
                            ucs_send.focus();
                        }
                    }
                    else
                        showErrNotification(err);
                    sendComment.send_lock = false;
                });
        }
    }
}

sendComment.send_lock = false;

function checkAuth(){
    ttSendMessage({type: "check authorisation state"}, function(response){

        if (!response.isConnected)
            showErrNotification('Отсутствует соединение с сервером');

        else if (!response.isAuth) {
            showErrNotification('Для этого действия необходимо авторизоваться');

            if(!$("div").is("#ucs_auth_wrap"))
                createAuthRegForm();
        }
        else
            sendComment();
    });
}

function clearCommentForm(callback){
    $(".ucs_overview").text('');

    response_to_id = null;
    $('#ucs_response_to_block').text('').removeClass('ucs_response_to_block_hover');

    $('#ucs_spam_button').css('display', 'none');
    $('#ucs_response_button').css('display', 'none');

    callback();
}

function showErrNotification(message){
    showNotification(message, '#ffefe8');
}

function showPositiveNotification(message){
    showNotification(message, '#dfffe8');
}

function showNotification(message, background){
    var ucs_info_bar = $('#ucs_info_bar');
    ucs_info_bar.css("background-color", background);
    ttCommentFormObj.setInfoBarTimeout(function(){ ucs_info_bar.empty() },5000);
    ucs_info_bar.html(message);
}

function showAuthErrNotification(message){
    var e = $('#ucs_auth_form_notification');
    var t = setTimeout(function(){ e.empty() },5000); //TODO: clearTimeout
    e.text(message);
}

function showRegErrNotification(message){
    var e = $('#ucs_reg_form_notification');
    var t = setTimeout(function(){ e.empty() },5000); //TODO: clearTimeout
    e.text(message);
}


function parseNewComments(new_comments){
    var len = new_comments.length;

    if (len !== 0){
        for(var i=0; i<len; i++){
            var new_comment = new_comments[i];
            createCommentBlock(new_comment._id, new_comment.from, new_comment.text, new_comment.time, new_comment.response_to, true);
        }
        ttCommentFormObj.showCommentNotification();
    }
    $('#ucs_main').data("plugin_tinyscrollbar").update('bottom');
}

chrome.runtime.onMessage.addListener(
    function(request) {

        switch (request.type){

            case "new comments":{
                parseNewComments(request.data);
                if (request.lock) ttCommentFormObj.setLock(request.lock);
            }
                break;

            case "show notification":
                showNotification(request.message);
                break;

            case "create form":{
                if($("div").is("#ucs_wrap")){ //если форма на странице есть, значит комменты остались

                    if(!request.leave) {

                        clearCommentForm(function(){//очищаем форму, для новых комментов, т.к. рум изменился
                            ttCommentFormObj.hideCommentNotification();

                            parseNewComments(request.data);
                        });
                    }
                }
                else createCommentForm(function(){//формы нет, создаем ее

                    if(request.username !== '') logInChangeForm(request.username);

                    parseNewComments(request.data);
                });
                ttCommentFormObj.setLock(request.lock);
            }
                break;

            case "changed auth state":{
                if (request.auth) logInChangeForm(request.username);
                else logOutChangeForm();
            }
                break;
        }
    });